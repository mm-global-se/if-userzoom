mmcore.IntegrationFactory.register(
  'Userzoom', {
    defaults : {
        isStopOnDocEnd: false
    },
    defaults: {
        isStopOnDocEnd: false,
        modifiers: {
            Persist: function(data, buildParams){
                if(data.persist && data.persistentCount > 0){
                    try {
                        var storedList = JSON.parse(mmcore.GetCookie('mm_iPersist', 1) || '[]');
                    } catch (e) {
                        var storedList = [];
                    }
                    var searchFor = data.campaign + '=';
                    for(var i = storedList.length; i--;){
                        if(!storedList[i].indexOf(searchFor))
                            storedList.splice(i, 1);
                    }
                    storedList.push(data.campaignInfo);
                    while(storedList.length > data.persistentCount)
                        storedList.shift();

                    mmcore.SetCookie('mm_iPersist', JSON.stringify(storedList), 365, 1);
                    data.campaignInfo = storedList.join('&');
                }
            }
        },
        persistentCount: 10
    },
    validate: function(data){
      if(!data.campaign)
          return 'No campaign.';
      return true;
    },
    check: function(data){ 
      return typeof window.trackHTML !== "undefined";
    },
    exec: function(data){   
        var uzClick = document.createElement('div');
        uzClick.style.cssText = 'visibility:hidden;width:0;height:0;';
        var prodSand = data.isProduction ? 'MM_Prod' : 'MM_QA';
        mvtData = "trackHTML" + "('" + prodSand + "_" + data.campaignInfo + "')";
        document.body.appendChild(uzClick).setAttribute("id", "uzMM");
        document.getElementById('uzMM').setAttribute("onclick", mvtData);
        document.getElementById("uzMM").click();
        //$("#uzMM").trigger( "click" ); 
        var deleteuzMM = document.getElementById('uzMM');
        deleteuzMM.parentNode.removeChild(deleteuzMM);
        if (data.callback) data.callback();
        return true;
    }
  }
);