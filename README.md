# UserZoom

---

[TOC]

## Introduction

Register and initialise UserZoom integration. limited 

## Download

* [UserZoom_register.js](https://bitbucket.org/mm-global-se/if-userzoom/src/master/src/UserZoom-register.js)

* [UserZoom_initialize.js](https://bitbucket.org/mm-global-se/if-userzoom/src/master/src/UserZoom-initialize.js)

## Ask client for

+ Campaign name

## Deployment instructions

### Content Campaign

+ Ensure that you have Integration Factory plugin deployed on site level and has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script with _order: -5_ and add the code from [UserZoom_register.js](https://bitbucket.org/mm-global-se/if-userzoom/src/master/src/UserZoom-register.js) into it

+ Create campaign script with _order: > -5_, then customise the code from [UserZoom_initialize.js](https://bitbucket.org/mm-global-se/if-userzoom/src/master/src/UserZoom-initialize.js) and add into it

### Redirect Campaign

Same process as Content Campaign with the addition of mapping the  integration initialisation script to both generation and redirect pages.

## QA

### Network Tab
Once a survey has been triggered, and the user is in the task section of the survey check the activities under Network tab. There should be a request for a new image. In Headers under Query String Parameters you should be able to see the experience data that you are sending to UserZoom.

![network_tab.png](https://bitbucket.org/repo/ny66nq/images/3922250953-network_tab.png)